FROM python:3.11.2-slim-bullseye

WORKDIR /src
COPY . /src

RUN python -m pip install --no-cache-dir virtualenv==20.21.0
